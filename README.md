# Projet de création d'un mappeur (sans gap) permettant la détection de Single-Nucleotide Polymorphism (SNPs).

Projet évalué dans le cadre de l'UE ALG.
Auteures : **BARRE Ève** et **FRABOULET Rose-Marie**.

## Explications des scripts index.py et map_snps.py :

### Indexation du génome de réference dans dumped_index.pd *(index.py)* :

Pour un génome de référence donné en entrée (au format .fasta), un FM-index sera généré. 

Une classe **Indexation** est définie avec :
* Un **constructeur init** est créé et possède plusieurs attributs : genome_file_path (génome de référence), genome (application de la méthode get_single_line sur ce génome) et fmi (obtention du FMI à partir de la méthode get_GM_index).

* Une méthode **str** qui renvoie le nombre de nucléotides dans la séquence de référence (après avoir appliqué la méthode get_single_line). 

* Une méthode **get_single_line** qui permet de modifier le contenu du fichier du génome de référence en une seule ligne. 

* Une méthode **get_FM_index** qui permet d'obtenir le FMI d'une séquence donnée. Cette méthode permet de créer :

	* A l'aide du fichier *tools_karkkainen_sanders.py* : 
		* Tableau des suffixes : FMI[0]

	* A l'aide du fihier *BWT.py* : 
		* Transformée de Burrows-Wheeler : FMI[1]
		* Nombre d'occurences de lettres dans le génome : FMI[2] 
		* Rang des lettres dans le génome : FMI[3]


* Une méthode **dump** qui permet de stocker le FMI dans un objet dump, afin d'être réutilisé dans le script map.py. 

* On retrouve dans le **main** les options du programme, gérées par getopt. 

* On instancie un objet de la classe Indexation en donnant comme argument le génome de référence.


### Mapping *(map_snps.py)* :

#### Ancrage : 

Une ancre est un k-mer, soit une séquence de taille k, qui s'aligne exactement sur le génome. 
- Pour chaque read, 
- pour chaque k-mer du read de taille k qui a au moins une occurrence dans le génome (get_occurrence()), 
- pour chaque position d'occurrence du k-mer qui ne correspond pas à une région d'un alignement déjà identifiée de ce read (les positions d'alignement sont stockées pour le read en cours), 
- on effectue une extension.


#### Extension : 

##### Les fonctions : 

###### Pour le mapping : 

* reverse():
Renvoie la séquence inversée et complémentaire d'un read.


* extend() :
Etend à gauche puis à droite l'alignement de l'ancre (un k-mer à l'alignement exact) sans gap et avec un nombre de substitution inférieur à la meilleure distance d'édition définie (best_h). 
	- Compte les substitutions au fur et à mesure de l'alignement (sub_num).
	- Retourne leurs positions dans le read (pos_sub). 
	--> la taille de cette liste donne leur nombre.
	- Ne retourne rien (None) si l'ensemble du read n'est pas aligné.
	- S'assure que le read ne contient pas de 'N' et que les valeurs d'entrées sont bien cohérentes.


* select_align() et traitement du multi-mapping :
Sélectionne un des deux alignements en entrée.
	- Ne retourne rien si les deux alignements à comparer sont "None". 
	- Si l'un des deux alignements est "None", retourne l'autre.
	- Retourne les informations de l'alignement qui a :
		- le moins de substitutions,
		- puis si égalité, celui qui a la position d'alignement dans le génome le plus à gauche.

* store_sub()
Ajoute les informations de substitutions de l'alignement d'un read dans le dictionnaire qui les stocke pour toutes les substitutions du génome d'intérêt par rapport au génome de référence. 
Ces informations stockées sont la position d'occurrence, la base référente, l'alternative et son abondance.
	- Compte progressivement l'abondance des substitutions. 

* mapping():
Donne les informations du meilleur alignement d'un read sur le génome de référence. 
	- Essaie une extension (extend()) à partir des occurrences de tous les k-mer pour un read donné dans le génome de référence. 
	- Met à jour la distance de hamming maximale permise lors de l’extension de l’alignement du read au nombre de substitution trouvé pour le meilleur alignement de la lecture courante jusqu’à ce calcul.
	- Passe à une position suivante d'occurrence du k-mer si  : 
		- donnerait un alignement au-delà de la taille du génome, 
		- cette position d'alignement du read a déjà été testée, 
		- l'extension de tout le read n'a pas été possible.
	- Définie cet alignement comme le meilleur si c'est le premier défini pour le read.
	- Si un alignement est déjà retenu pour ce read, conserve le meilleur selon select_align().
	- Ne retourne rien si aucun alignement n'est retenu pour le read.  

* follow_process():
Permet l'affichage au cours de l'exécution qu'un certain nombre d'alignements de read est bien effectué. Cela 4 fois par puissance de 10 de l'indice du read pour lequel l'affichage est effectué. De plus, stocke la taille de ce read dans une liste pour permettre le calcul d'une moyenne de taille de reads échantillonnés du jeu de données. 

###### Les fonctions tests : 

* get_index()
Fournis les données d'indexation d'un petit génome pour fournir des exemples. 

* test_reverse()
* test_extend()
* test_select_align()
* test_mapping()
* test_store_sub()


##### Déroulement :

- Récupère les informations des arguments mentionnés par l'utilisateur. 
- Reprend les informations sur le génome indexé. 
- Vérifie que le génome ne contient pas de "N".
- Met le génome sur une seule ligne à partir du fichier fasta.
- À la volée par la lecture du fichier fasta des reads : 
	- Fonctionne si et seulement si le read est sur une ligne,
	- Génère le meilleur alignement en forward,
	- Si cet alignement en forward contient des substitutions ou est None, genère aussi le meilleur alignement en reverse à partir de mapping(),
	- Sélectionne le meilleur des deux à partir de select_align(),
	- Met à jour le dictionnaire des substitutions avec store_sub().
- Remplie le fichier de sortie contenant les informations sur les substitutions portées par le génome d'intérêt (position sur le génome de référence, base référente, base alternative et son abondance)
- Pour aider le choix du paramètre "m" d'abondance minimale autorisée des substitutions, affiche les proportions de chacune des abondances retenues. 
- Pour aider l'interprétation, affiche le pourcentage d'identité du génome d'intérêt avec le génome de référence, le nombre de substitution retenue et la taille du génome de référence. 
- Pour faciliter le suivi du calcul, affiche progressivement le temps auquel l'alignement des reads commence et termine. 
- Affiche la durée d'éxecution du code en secondes et minutes.


