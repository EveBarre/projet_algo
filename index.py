#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Subject : ALG project
Autor : BARRE Eve et FRABOULET Rose-Marie

This script index the genome of reference as a FM-index.
The commande used is : "python index.py --ref [genome_file.fa] --out [dumped_index.dp]"
'''

# Imports modules of functions that will be used for indexing

import getopt  # Manages program options
import sys  # Indicates arguments to execute this script
import pickle  # Python object
from typing import Tuple, List, Dict  # Import types
import BWT  # Gives functions to apply pattern matching
import tools_karkkainen_sanders as tks  # Suffix array


class Indexation:
    '''
    This class allows to index the genome of reference.
    '''
    def __init__(self, genome_file_path: str):
        '''
        Initializes the Indexation class.

        Args:
            genome_file_path (str): path to the reference genome file
        '''

        self.genome_file_path: str = genome_file_path
        self.genome: str = self.get_single_line()
        self.fmi: Tuple(List[int], str, Dict[str, int], List[int]) = self.get_FM_index()

    def __str__(self):
        '''
        Displays the number of nucleotides of the reference genome sequence and the name of the file which composes it.

        Returns:
            The length of nucleotides of the reference genome sequence file.
        '''

        return f"The reference genome '{self.genome_file_path}' is a sequence of {str(len(self.genome)-1)} nucleotides."

    def get_single_line(self) -> str:
        '''
        Modification of genome_file.fasta file into a single line.

        Returns:
            The reference genome in a single line (str).
        '''

        genome_single_line = ""
        with open(self.genome_file_path, "r") as file_descriptor:
            for line in file_descriptor:
                if line[0] == ">":
                    continue
                genome_single_line += line.rstrip("\n")
            genome_single_line = genome_single_line + "$"  # add "$" at the end of the sequence to perform the FM index
        return genome_single_line

    def get_FM_index(self) -> tuple:
        '''
        Create the FM-index, composed of the suffix array (sa), the Burrows-Wheeler Transform (bwt), the number of occurence of letters in the genome (n) and the rank of the letters (r).

        Returns:
            The FM-index of the reference genome sequence (Tuple(List[int], str, Dict[str, int], List[int])).
        '''

        sa = tks.simple_kark_sort(self.genome)
        bwt = BWT.get_bwt(self.genome, sa)
        r, n = BWT.get_r_n(bwt)
        assert('N' not in n.keys()), "This mapper works only without 'N' in the reference genome sequence."
        fmi = (sa, bwt, n, r)
        return fmi

    def dump(self, dumped_index_file_path: str):
        '''
        Create an object of the FM-index of the reference genome sequence.

        Args:
            dumped_index_file_path (str): the output file which contains FM-index of the reference genome sequence.
        '''

        with open(dumped_index_file_path, "wb") as file_descriptor:
            pickle.dump(self.fmi, file_descriptor)


if __name__ == "__main__":

    user_genome_file_path: str = ""
    user_dumped_index_file_path: str = ""

    try:
        opts, args = getopt.getopt(sys.argv[1:], "hr:o:", ["ref=", "out="])
    except getopt.GetoptError:
        print("Requested command : index.py --ref [genome_file.fa] --out [dumped_index.dp]")
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("index.py --ref [genome_file.fa] --out [dumped_index.dp]")
            sys.exit(0)
        elif opt in ("-r", "--ref"):
            user_genome_file_path = arg
        elif opt in ("-o", "--out"):
            user_dumped_index_file_path = arg
    print("Input file is", user_genome_file_path)
    print("Output file is", user_dumped_index_file_path)

    indexation = Indexation(user_genome_file_path)
    print(indexation)
    indexation.dump(user_dumped_index_file_path)
