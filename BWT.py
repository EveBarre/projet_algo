#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Subject : ALG project
Autor : BARRE Eve et FRABOULET Rose-Marie

File obtained by TP2 correction given by teachers and modified by ourselves.
'''

from typing import List, Dict  # Import types


def get_bwt(s: str, sa: List[int]) -> str:
    """
    Returns the Burrows-Wheeler Transform of the sequence (s) whose suffix array (sa) has been computed.

    Args:
        s (str): sequence
        sa (List[int]): suffix array

    Returns:
        bwt (str): Burrows-Wheeler Transform of the sequence
    """

    bwt = ""
    for pos in sa:
        if pos == 0:
            bwt += "$"
        else:
            bwt += s[pos-1]
    return bwt


def get_r_n(bwt: str) -> (List[int], Dict[str, int]):
    """
    Returns the rank of letters of the sequence (s) in bwt and the number of occurrence of the letters of the sequence (s) with the help of Burrows Wheeler Transform.

    Args:
        bwt (str): Burrows-Wheeler Transform

    Returns:
        r (List[int]): rank of letters in bwt
        n (Dict[str, int]): number of occurence of letters in the sequence (s)
    """

    n = {}
    r = []
    for letter in bwt:
        if letter not in n:
            n[letter] = 0
        n[letter] += 1
        r.append(n[letter])
    return r, n


def left_first(alpha: str, k: int, n: Dict[str, int]) -> int:
    '''
    Returns the position of the kth letter alpha appears.

    Args:
        alpha (str): letter we are looking for
        k (int): kième letter
        n (Dict[str, int]): number of occurence of letters in the sequence (s)

    Returns:
        The position where the kth letter alpha appears.

    '''
    assert k <= n[alpha], f"Cannot ask for the {k}^th {alpha}, it does not exist."
    if alpha == "$":
        return 0
    if alpha == "A":
        return n["$"] + k - 1
    if alpha == "C":
        return n["$"] + n["A"] + k - 1
    if alpha == "G":
        return n["$"] + n["A"] + n["C"] + k - 1
    if alpha == "T":
        return n["$"] + n["A"] + n["C"] + n["G"] + k - 1
    raise ValueError(f"Character {alpha} not in the bwt")


def get_down(bwt: str, alpha: str, start: int, stop: int) -> int:
    """
    Detects the first occurrence of alpha in bwt for i in [start, stop].

    Args:
        bwt (str): burrows wheeler transform
        alpha (str): letter we are looking for
        start (int): start position of the interval
        stop (int): stop position of the interval

    Returns:
        From start go down in the bwt as long as bwt[line] != alpha and line <= stop :
        - if bwt[line] == alpha, returns the corresponding line
        - if line > stop: returns -1
    """
    line = start
    while line <= stop:
        if bwt[line] == alpha:
            return line
        line += 1
    return -1


def get_up(bwt: str, alpha: str, start: int, stop: int) -> int:
    '''
    Detects the first occurrence of alpha in bwt for i in [stop, start].

    Args:
        bwt (str): burrows wheeler transform
        alpha (str): letter we are looking for
        start (int): start position of the interval
        stop (int): stop position of the interval

    Returns:
        From stop go up in the bwt as long as bwt[line] != alpha and line >= start :
      - if bwt[line] == alpha, returns the corresponding line
      - if line < start: returns -1
    '''
    line = stop
    while line >= start:
        if bwt[line] == alpha:
            return line
        line -= 1
    return -1


def get_occurrences(pattern: str, bwt: str, n: Dict[str, int], r: List[int], sa: List[int]) -> List[int]:
    """
    Returns pattern occurrences in the text coded in the bwt.

    Args:
        pattern(str): pattern we are looking for in the sequence (s)
        bwt (str): burrows wheeler transform
        n (Dict[str, int]): number of occurence of letters in the sequence (s)
        r (List(int)): rank of letters in bwt
        sa (List(int)): suffix array

    Returns:
        The list of occurences of the pattern in the sequence (s).
    """
    start = 0
    stop = len(bwt)-1
    # read the pattern from right to left
    for pos_pattern in range(len(pattern)-1, -1, -1):
        current_char = pattern[pos_pattern]
        new_start = get_down(bwt, current_char, start, stop)
        if new_start == -1:
            return []
        new_stop = get_up(bwt, current_char, start, stop)
        start = left_first(bwt[new_start], r[new_start], n)
        stop = left_first(bwt[new_stop], r[new_stop], n)
    res = []
    for i in range(start, stop+1):
        res.append(sa[i])
    return res
