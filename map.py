#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Subject : ALG project
Autor : BARRE Eve et FRABOULET Rose-Marie

This script maps reads on a referenced genome allowing no gap and a number of substitutions lower to the value h.
It returns a list of substitutions on the genome of interest that have a minimum of abundance on reads at the value m.

It works with nucleotide sequences without "N" and only with sequencing data of DNA.

The commande used is like :
"python map.py --ref [genome_file.fa] --index [dumped_index.dp] --reads [reads.fa] -k [k_value] --max_hamming [h_value] --min_abundance [m_value] --out snps.vcf".
'''

# Imports modules of functions that will be used for mapping

import getopt  # Manages program options
import sys  # Indicates arguments to execute this script
import pickle  # Python object
from typing import List, Dict  # Import types
import time  # Execution time
import BWT  # Gives functions to apply pattern matching
import tools_karkkainen_sanders as tks  # Suffix array


def reverse(read: str)-> str:
    '''
    Returns the complementary reversed sequence of a read.

    Args:
        read (str): sequence of the read to reverse

    Returns:
        read_reversed (str): the sequence reversed
    '''

    read = read[::-1]
    read_reversed = ""

    for letter in read:
        if letter == "A":
            read_reversed += "T"
        elif letter == "T":
            read_reversed += "A"
        elif letter == "C":
            read_reversed += "G"
        elif letter == "G":
            read_reversed += "C"
        else:
            raise ValueError(f"Letters of reads should be A, T, G or C and not {letter}")
    return read_reversed


def extend(start_k: int, stop_k: int, read: str, pos_g: int, seq_g: str, h: int, k: int, i_r: int) -> List[int]:
    '''
    Extends the alignment of a kmer (of a size of k and at position [start_k:stop_k] in the read) in the reference sequence allowing a number of subsitutions lower than h.
    Counts substitutions and stores theirs positions on the read.

    Args:
        start_k (int):  position of kmer's first letter
        stop_k (int): position of kmer's last letter
        read (str): read's sequence
        pos_g (int): position of kmer's occurence in the genome
        seq_g (str): genome of reference's sequence
        h (int): maximum of substitutions allowed
        k (int): size of kmer
        i_r (int): number (cardinal) of the current read

    Returns: (List[int])
        [0] position of the alignment in the genome of reference
        [1] positions of substitutions in the read in a list
        The size of this list indicates indirectly the number of substitutions
    '''

    sub_num = 0  # Number of substitutions
    pos_sub = []  # Positions of reads substitutions
    ext_g = 0  # Size of extension on the left of the kmer
    ext_d = 0  # Size of extension on the right of the kmer

    # Verifies if input data are consistents
    if (stop_k - start_k + 1) != k:
        raise ValueError(f"Parameters do not check this equation : (stop_k - start_k + 1) == k --> {stop_k} - {start_k} + 1) == {k}.")
    if start_k > stop_k:
        raise ValueError(f"start_k < stop_k is not verified\nstart_k : {start_k}\nstop_k : {stop_k}.")
    if stop_k > len(read):
        raise ValueError(f"stop_k < len(read) is not verified\nstop_k : {stop_k}\nread's size : {len(read)}.")
    if len(read) > len(seq_g):
        raise ValueError(f"len(read) < len(seq_g) is not verified\ngenome's size : {len(seq_g)}\nread's size : {len(read)}.")
    if k > len(read):
        raise ValueError(f"k < len(seq_g) is not verified\nk : {k}\nread's size : {len(read)}.")

    # Extension on the left
    while start_k != 0:
        start_k -= 1
        ext_g += 1

        if read[start_k] == "N":
            raise ValueError(f"This mapper works only without 'N' in sequences. 'N' occurs in the read {i_r} at position {read[start_k]}.")

        if read[start_k] != seq_g[pos_g - ext_g]:  # Compares letters
            sub_num += 1
            pos_sub.append(start_k)
            if sub_num > h:
                return None

    # Extension on the right
    while stop_k != (len(read)-1):
        stop_k += 1
        ext_d += 1

        if read[stop_k] == "N":
            raise ValueError(f"This mapper works only without 'N' in sequences. 'N' occurs in the read {i_r} at position {read[stop_k]}.")

        if read[stop_k] != seq_g[pos_g + k + ext_d - 1]:
            sub_num += 1
            pos_sub.append(stop_k)
            if sub_num > h:
                return None

    assert(ext_d + ext_g + k) == len(read), f"Error on variables :\nthe equation '(ext_d + ext_g + k) == len(read)' is not verified.\next_d : {ext_d}\next_g : {ext_g}\nk : {k}\nread's size : {len(read)}"

    return [(pos_g - ext_g), pos_sub]  # Position of alignment in the genome and positions' list of substitutions


def select_align(align_1: [int, [int]], align_2: [int, [int]]) -> [int, [int]]:
    '''
    Selects the alignment with less substitutions and left-most in the genome.

    Args:
        align_1 (List(int, List(int))): start position of the mapping in the genome and the list of substitutions positions
        align_2 (List(int, List(int))): the older best alignment or another alignment to compare

    Returns: (List(int, List(int)))
        The best alignment bewteen the both.
        None if there is no alignment.
    '''

    if align_1 is None and align_2 is None:
        return None

    if align_2 is None:
        return align_1

    if align_1 is None:
        return align_2

    if len(align_1[1]) < len(align_2[1]):  # Compares number of substitutions
        return align_1

    if len(align_1[1]) == len(align_2[1]) and align_1[0] < align_2[0]:  # Compares position of alignment, selects the lower (more on the left)
        return align_1

    return align_2


def store_sub(read: str, align: [int, [int], str], dict_snps: {int: {str: int}}) -> {}:
    '''
    Updates the dictionnary that stores information on substitutions (alternative, reference base,  their position of occurence and their abundance) with possible curent substitutions.

    Args:
        read (str): current read aligned
        align ([int, [int], str]): best alignement of the current read
        dict_snps ({int: {str: int}}): current dictionnary of substitutions

    Returns:
        Dictionnary of substitutions updated.
    '''

    if align[2] == "-":  # If the read is mapped in reverse.
        read = reverse(read)

    for c_pos_sub in align[1]:
        c_pos_sub_g = align[0] + c_pos_sub
        # Corresponds to the position in the genome of the substitution (position of occurence of the read in the genome + position of the substitution in the read).
        if c_pos_sub_g not in dict_snps.keys():
            dict_snps[c_pos_sub_g] = {read[c_pos_sub]: 1}

        elif read[c_pos_sub] not in dict_snps[c_pos_sub_g].keys():  # If this alternative base not occurs yet
            dict_snps[c_pos_sub_g][read[c_pos_sub]] = 1  # Add one to the adundance counter

        else:
            dict_snps[c_pos_sub_g][read[c_pos_sub]] += 1  # Add one to the adundance counter

    return dict_snps


def mapping(c_read: str, bwt: str, sa: List[int], n: Dict[str, int], r: List[int], k: int, genome: str, h: int, i_r: int, sens: str) -> [int, int, str]:
    '''
    Returns the best alignement of the read (forward or reverse in input).

    Args:
        c_read (str): current read to map
        Information about the indexed genome:
            bwt (str): burrows wheeler transform
            sa (List[int]): suffix array
            n (Dict[str, int]): number of occurence of letters in the sequence (s)
            r (List[int]): rank of each letter for letter in bwt
        k (int): size of kmer
        genome (str): sequence of the genome of reference
        h (int): maximum number of substitutions allowed
        i_r (int): index of the current read
        sens (str): "+" if the read is forward, "-" if it is reverse

    Returns:
        If the read aligns with the genome, for the best alignment ([int, int, str]):
            [0] start position in the genome of the alignment the genome
            [1] positions of substitutions in the read
            [2] direction of the read (sens) = "+" if the read is forward, "-" if it is reverse
        None if there is no alignment.
    '''

    all_pos_map = []  # Stores the different mapping positions performed for this read in this direction in order not to redo the calculations if a next kmer is positioned at the same place
    best_map = []  # Checks later if there is an alignment
    best_h = h  # To stop try to align if the distance of hamming of the current best align is lower

    for ik in range(len(c_read) - k + 1):
        for c_pos_kmer_g in BWT.get_occurrences(c_read[ik: ik + k], bwt, n, r, sa):  # Tries all the positions of occurrence of the kmer in the genome

            # Verifies that occurrences of the kmer in the genome are not out of range
            if c_pos_kmer_g > (len(genome) - len(c_read) + ik + 1):
                continue

            pos_map = (c_pos_kmer_g - ik)  # Initial position in the genome of the current read

            if pos_map in all_pos_map:
                continue

            align = extend(ik, (ik + k - 1), c_read, c_pos_kmer_g, genome, best_h, k, i_r)  # Alignment position of the read on the genome

            if align is None:
                continue  # Switch to an other position because this alignment needed too much substitutions

            assert(pos_map == align[0]), f"This equation is not verified : pos_map ({pos_map}) == align[0] ({align[0]})"

            if all_pos_map == []:  # If there is no alignment of this read yet
                all_pos_map.append(pos_map)
                best_map = align
                best_h = len(align[1]) # Update the maximum of substitution allowed at what the best_align found until there
                # The best alignment to which we will compare if the read aligns in several places
                    # best_map[0] : position of the alignment
                    # best_map[1] : list of substitution positions
                continue

            all_pos_map.append(pos_map)

            if best_h == len(align[1]) and best_map[0] < align[0]:
                # If current alignement have as much substitutions as the best alignment but is more at left in the genome of reference, concerves the best_align
                continue

            best_map = align
            best_h = len(align[1])

    if best_map == []:
        return None

    best_map.extend(sens)

    return best_map


def follow_process(i_r: int, c_read: str, list_read_size: [int], t0: (int))-> str:
    '''
    Print a sentence regularly to follow the process of mapping.
    As the number of reads can varies, print 4 times for each power of 10.
    Stores the length of the reads for the one a sentence is printed in a list to calculate an average of read's size sampled.

    Args:
        i_r (int): index of the current read
        c_read (str): current read
        list_read_size ([int]): list of read size calculated
        t0 (int): time at the one the process started

    Returns:
        A sentence which describes the number of aligned reads in time.
    '''

    for limit in range(6):  # Defines a limit at 10^6
        if i_r < 10**limit:
            if i_r % (2 * (10**(limit-1))) == 0:
                list_read_size.append(len(c_read))
                return print(f"Alignement of {i_r} reads done at {round(time.time() - t0, 3)} seconds.")


# Functions for tests : test_driven development / non-regression test

def get_index(genome: str) -> [str, list, dict, list]:
    '''
    To have a indexation of a genome to use as an example in test_functions.

    Args:
        genome (str): genome to test

    Returns:
        In a list ([str, list, dict, list]) :
            [0] bwt (str)
            [1] sa (List[int]))
            [2] n (Dict[str, int])
            [3] r (List[int])
    '''

    sa = tks.simple_kark_sort(genome)
    bwt = BWT.get_bwt(genome, sa)
    r, n = BWT.get_r_n(bwt)

    return [bwt, sa, n, r]


def test_reverse():
    '''
    Test the function reverse() with small data.
    '''
    read = "AAATGCGTC"

    assert(reverse(read) == "GACGCATTT"), f"Unfailed reverse function."

    print(f"test_reverse() \033[92mpassed\033[0m.")


def test_extend():
    '''
    Test the function extend() with small data.
    '''

    # Checks that the substitution is in the extension
    start_k = 2
    stop_k = 6
    read = "CCAGACCGCAG"  # Substitutions at 0 and 1 position
    pos_g = 2
    seq_g = "GGAGACCGCAGGCTGCGCGCGTATCGCAGCATCTGGCATTACGCC"
    h = 2
    k = 5
    i_r = 2

    read = "CCAGACCGCAC" # 3 substitutions at positions 0, 1 and 10 (can't be inside the kmer)
    assert not extend(start_k, stop_k, read, pos_g, seq_g, h, k, i_r), f"The number of substitution is outside threshold {h}, extend should be None."

    read = "GGCATTACGCC" # no substitutions
    pos_g = 36
    assert(extend(start_k, stop_k, read, pos_g, seq_g, h, k, i_r)) == [34, []], f"Issue to aligne at the end of the genome."

    print(f"test_extend() \033[92mpassed\033[0m.")


def test_select_align():
    '''
    Test the function select_align().
    '''
    assert(select_align([5, [1, 5]], [5, [1, 5, 6]])) == [5, [1, 5]], f"The selection of the best alignment failed on the substitution number."
    assert(select_align([4, [1, 5]], [5, [1, 5]])) == [4, [1, 5]], f"The selection of the best alignment failed on the alignement position."

    print(f"Test_select_align() \033[92mpassed\033[0m.")


def test_mapping():
    '''
    Test the function mapping() with small data.
    '''

    # Don't forget to have a "$" at the end of the genome's sequence.
    genome = "CTGTCAGTTAGTGGTTCAGT$"
    read = "GTCTTTTAGT"  # GTCAGTTAGT is without substitutions

    k = 4
    h = 2
    i_r = 5
    assert(mapping(read, get_index(genome)[0], get_index(genome)[1], get_index(genome)[2], get_index(genome)[3], k, genome, h, i_r, "+")) == [2, [4, 3], '+'], f"A AJOUTER"


    # Tests for size data n°2 : for the read 4

    # Genome 1
    genome = "CGAGCTGGTCCTAACCCGGAGACCGCAGGCTGCGCGCGTATCGCAGCATCTGGCATTACGCCGCATCGAGTGCATGCACGAGAGAAGGAAGGGCACTGTTCGCTACCAGTCTACCCTACATAAGATTATACACATCTTTGAGTTTTTTCGTCCATCAAGTAGCGAAACGGATGTAGCGCTTCCCGACGGACTTCATAGGCGATTCACTCACGGTCGATTGAGCCGGGCGGAGCATGCTACACGTGTAAATGTTCTCGGTTAACTATTATGGTTTGGATGATTGGTGCCAGTGTTGCTTGCGTCTGACGAGTACACACCCTATAGAGAAAGAATACCTCATGTTTGCGTAACGAGCGTTCAATTTCCTCCTGTTTGTACCTTACCCCGAGGGTTATCGAACCTTGCGGGCTGGGTCGGAAAACTTGTCTTAGAGGCCTGCGACCGTGATTACATTGCTACAGATTGTCCCCATTGTTCCGCGGAGGCATTTTCGCAGGACGTCTGATGTAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGCTATGTTTAGAGTAATAGTTTCCAACCCACTGAGGCACTCTCGTTCTGTGAAACAGTTAGGCCGGTTGCCGTGCGCAGCAACATGGTGTGGACACATCTCCCAGCCTGTTGAATGACGGCCTAGTGTCAGGAATTAGAGAGCCTTAACCCTATCAGGGTTGTCCCGACAGTTGACATCGCCCGAGATGGCTCTTTTGAAGGGCCCCAAGATCGGCTGCATCTACTTGGCACAACGGCTTTGCCTGGCTCGTTAAAATCCTGTCACATACGCGAGTTCCCGAAGTTGGCCGATTGCCCCTATCACCGTGTTGGAACCCATGTGTTAGCACAGACCTGAAGACTAATCCTCATTCCCTGTGTCACCGCAATTTCAGCCAAGCCAGCCACGCGCTCCTTGTTAGTCGTATATGGCGTTAATGAGCTTCAAACCCCGA$"
    read = "CTCAGTGGGTTGGAAACTATTACTCTAAACATAGCTAGATCGCAGTGAAGTGATCAATCAAATTATGCCTATCCTTCAAGGGAAACCGCATTACATCAGA"

    assert(mapping(reverse(read), get_index(genome)[0], get_index(genome)[1], get_index(genome)[2], get_index(genome)[3], k, genome, h, i_r, "-") == [500, [], '-']), f"Error while mapping a reverse read."

    # Genome 2
    genome = "CGAGCTGGTCCTAACCCGGAGACCGCAGGCTGCGCGCGTATCGCAGCATCTGGCATTACGCCGCATCGAGTGCATGCACGAGAGAAGGAAGGGCACTGTTCGCTACCAGTCTACCCTACATAAGATTATACACATCTTTGAGTTTTTTCGTCCATCAAGTAGCGAAACGGATGTAGCGCTTCCCGACGGACTTCATAGGCGATTCACTCACGGTCGATTGAGCCGGGCGGAGCATGCTACACGTGTAAATGTTCTCGGTTAACTATTATGGTTTGGATGATTGGTGCCAGTGTTGCTTGCGTCTGACGAGTACACACCCTATAGAGAAAGAATACCTCATGTTTGCGTAACGAGCGTTCAATTTCCTCCTGTTTGTACCTTACCCCGAGGGTTATCGAACCTTGCGGGCTGGGTCGGAAAACTTGTCTTAGAGGCCTGCGACCGTGATTACATTGCTACAGATTGTCCCCATTGTTCCGCGGAGGCATTTTCGCAGGACGTCTGATGTAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGCTATGTTTAGAGTAATAGTTTCCAACCCACTGAGGCACTCTCGTTCTGTGAAACAGTTAGGCCGGTTGCCGTGCGCAGCAACATGGTGTGGACACATCTCCCAGCCTGTTGAATGACGGCCTAGTGTCAGGAATTAGAGAGCCTTAACCCTATCAGGGTTGTCCCGACAGTTGACATCGCCCGAGATGGCTCTTTTGAAGGGCCCCAAGATCGGCTGCATCTACTTGGCACAACGGCTTTGCCTGGCTCGTTAAAATCCTGTCACATACGCGAGTTCCCGAAGTTGGCCGATTGCCCCTATCACCGTGTTGGAACCCATGTGTTAGCACAGACCTGAAGACTAATCCTCATTCCCTGTGTCACCGCAATTTCAGCCAAGCCAGCCACGCGCTCCTTGTTAGTCGTATATGGCGTTAATGAGCTTCAAACCCCGACGAGCTGGTCCTAACCCGGAGACCGCAGGCTGCGCGCGTATCGCAGCATCTGGCATTACGCCGCATCGAGTGCATGCACGAGAGAAGGAAGGGCACTGTTCGCTACCAGTCTACCCTACATAAGATTATACACATCTTTGAGTTTTTTCGTCCATCAAGTAGCGAAACGGATGTAGCGCTTCCCGACGGACTTCATAGGCGATTCACTCACGGTCGATTGAGCCGGGCGGAGCATGCTACACGTGTAAATGTTCTCGGTTAACTATTATGGTTTGGATGATTGGTGCCAGTGTTGCTTGCGTCTGACGAGTACACACCCTATAGAGAAAGAATACCTCATGTTTGCGTAACGAGCGTTCAATTTCCTCCTGTTTGTACCTTACCCCGAGGGTTATCGAACCTTGCGGGCTGGGTCGGAAAACTTGTCTTAGAGGCCTGCGACCGTGATTACATTGCTACAGATTGTCCCCATTGTTCCGCGGAGGCATTTTCGCAGGACGTCTGATGTAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGCTATGTTTAGAGTAATAGTTTCCAACCCACTGAGGCACTCTCGTTCTGTGAAACAGTTAGGCCGGTTGCCGTGCGCAGCAACATGGTGTGGACACATCTCCCAGCCTGTTGAATGACGGCCTAGTGTCAGGAATTAGAGAGCCTTAACCCTATCAGGGTTGTCCCGACAGTTGACATCGCCCGAGATGGCTCTTTTGAAGGGCCCCAAGATCGGCTGCATCTACTTGGCACAACGGCTTTGCCTGGCTCGTTAAAATCCTGTCACATACGCGAGTTCCCGAAGTTGGCCGATTGCCCCTATCACCGTGTTGGAACCCATGTGTTAGCACAGACCTGAAGACTAATCCTCATTCCCTGTGTCACCGCAATTTCAGCCAAGCCAGCCACGCGCTCCTTGTTAGTCGTATATGGCGTTAATGAGCTTCAAACCCCGA$"

    assert(mapping(reverse(read), get_index(genome)[0], get_index(genome)[1], get_index(genome)[2], get_index(genome)[3], k, genome, h, i_r, "-") == [500, [], '-']), f"Error for choosing the best alignment."

    # Genome 3
    genome = "CGAGCTGGTCCTAACCCGGAGACCGCAGGCTGCGCGCGTATCGCAGCATCTGGCATTACGCCGCATCGAGTGCATGCACGAGAGAAGGAAGGGCACTGTTCGCTACCAGTCTACCCTACATAAGATTATACACATCTTTGAGTTTTTTCGTCCATCAAGTAGCGAAACGGATGTAGCGCTTCCCGACGGACTTCATAGGCGATTCACTCACGGTCGATTGAGCCGGGCGGAGCATGCTACACGTGTAAATGTTCTCGGTTAACTATTATGGTTTGGATGATTGGTGCCAGTGTTGCTTGCGTCTGACGAGTACACACCCTATAGAGAAAGAATACCTCATGTTTGCGTAACGAGCGTTCAATTTCCTCCTGTTTGTACCTTACCCCGAGGGTTATCGAACCTTGCGGGCTGGGTCGGAAAACTTGTCTTAGAGGCCTGCGACCGTGATTACATTGCTACAGATTGTCCCCATTGTTCCGCGGAGGCATTTTCGCAGGACGTCTGATGTAATGCGGTTTCCCTTGAAGGATAGGCATAATTTGATTGATCACTTCACTGCGATCTAGCTATGTTTAGAGTAATAGTTTCCAACCCACTGAGGCACTCTCGTTCTGTGAAACAGTTAGGCCGGTTGCCGTGCGCAGCAACATGGTGTGGACACATCTCCCAGCCTGTTGAATGACGGCCTAGTGTCAGGAATTAGAGAGCCTTAACCCTATCAGGGTTGTCCCGACAGTTGACATCGCCCGAGATGGCTCTTTTGAAGGGCCCCAAGATCGGCTGCATCTACTTGGCACAACGGCTTTGCCTGGCTCGTTAAAATCCTGTCACATACGCGAGTTCCCGAAGTTGGCCGATTGCCCCTATCACCGTGTTGGAACCCATGTGTTAGCACAGACCTGAAGACTAATCCTCATTCCCTGTGTCACCGCAATTTCAGCCAAGCCAGCCACGCGCTCCTTGTTAGTCGTATATGGCGTTAATGAGCTTCAAACCCCGATCGGGGTTTGAAGCTCATTAACGCCATATACGACTAACAAGGAGCGCGTGGCTGGCTTGGCTGAAATTGCGGTGACACAGGGAATGAGGATTAGTCTTCAGGTCTGTGCTAACACATGGGTTCCAACACGGTGATAGGGGCAATCGGCCAACTTCGGGAACTCGCGTATGTGACAGGATTTTAACGAGCCAGGCAAAGCCGTTGTGCCAAGTAGATGCAGCCGATCTTGGGGCCCTTCAAAAGAGCCATCTCGGGCGATGTCAACTGTCGGGACAACCCTGATAGGGTTAAGGCTCTCTAATTCCTGACACTAGGCCGTCATTCAACAGGCTGGGAGATGTGTCCACACCATGTTGCTGCGCACGGCAACCGGCCTAACTGTTTCACAGAACGAGAGTGCCTCAGTGGGTTGGAAACTATTACTCTAAACATAGCTAGATCGCAGTGAAGTGATCAATCAAATTATGCCTATCCTTCAAGGGAAACCGCATTACATCAGACGTCCTGCGAAAATGCCTCCGCGGAACAATGGGGACAATCTGTAGCAATGTAATCACGGTCGCAGGCCTCTAAGACAAGTTTTCCGACCCAGCCCGCAAGGTTCGATAACCCTCGGGGTAAGGTACAAACAGGAGGAAATTGAACGCTCGTTACGCAAACATGAGGTATTCTTTCTCTATAGGGTGTGTACTCGTCAGACGCAAGCAACACTGGCACCAATCATCCAAACCATAATAGTTAACCGAGAACATTTACACGTGTAGCATGCTCCGCCCGGCTCAATCGACCGTGAGTGAATCGCCTATGAAGTCCGTCGGGAAGCGCTACATCCGTTTCGCTACTTGATGGACGAAAAAACTCAAAGATGTGTATAATCTTATGTAGGGTAGACTGGTAGCGAACAGTGCCCTTCCTTCTCTCGTGCATGCACTCGATGCGGCGTAATGCCAGATGCTGCGATACGCGCGCAGCCTGCGGTCTCCGGGTTAGGACCAGCTCG$"

    assert(mapping(reverse(read), get_index(genome)[0], get_index(genome)[1], get_index(genome)[2], get_index(genome)[3], k, genome, h, i_r, "-") == [500, [], '-']), f"Error for choosing the best alignment." # A revoir

    print(f"test_mapping() \033[92mpassed\033[0m.")


def test_store_sub():
    '''
    Test the function store_sub().
    '''

    print(f"test_store_sub() \033[92mpassed\033[0m.")

# Function test :

# print("Function test :\n")
# test_extend()
# test_mapping()
# test_reverse()
# test_select_align()
# test_store_sub()


def main():
    genome_file = ""  # Genome file
    FMI_file = ""  # Elements of genome's indexation
    reads = ""  # Reads
    k = ""  # Kmer size for seed step
    h = ""  # Max_hamming
    m = ""  # Min_abundance
    snps = ""  # Single Nucleotides Polymorphism

    dict_snps = {}  # {int:{str: int}}
    # Keys are position of the substitution in the genome
    # Values are alternative based and their current abundance in another dictionnary


    try:
        opts, args = getopt.getopt(sys.argv[1:], "hr:i:rd:k:max:min:o:", ["ref=", "index=", "reads=", "k_value=", "max_hamming=", "min_abundance=", "out="])
    except getopt.GetoptError:
        print("map.py --ref [genome_file.fa] --index [dumped_index.dp] --reads [reads.fa] -k [k_value] --max_hamming [h_value] --min_abundance [m_value] --out snps.vcf")
        sys.exit(2)
    for opt, arg in opts:
        if opt == "-h":
            print("map.py --ref [genome_file.fa] --index [dumped_index.dp] --reads [reads.fa] -k [k_value] --max_hamming [h_value] --min_abundance [m_value] --out snps.vcf")
            sys.exit()
        elif opt in ("-r", "--ref"):
            genome_file = arg
        elif opt in ("-i", "--index"):
            FMI_file = arg
        elif opt in ("-rd", "--reads"):
            reads = arg
        elif opt in ("-k", "--k_value"):
            k = int(arg)
        elif opt in ("-max", "--max_hamming"):
            h = int(arg)
        elif opt in ("-min", "--min_abundance"):
            m = int(arg)
        elif opt in ("-o", "--out"):
            snps = arg

    with open(FMI_file, "rb") as file_descriptor:
        FMI = pickle.load(file_descriptor)

    sa = FMI[0]
    bwt = FMI[1]
    n = FMI[2]
    r = FMI[3]

    with open(genome_file, "r") as file_descriptor:
        genome = ""
        for line in file_descriptor:
            if line[0] == ">":
                continue
            genome += line.rstrip("\n")

    # Initialization of the snps output file
    with open("snps.vcf", "w") as snps:
        snps.write(f"#REF: {genome_file}\n")
        snps.write(f"#READS: {reads}\n")
        snps.write(f"#K: {k}\n")
        snps.write(f"#MAX_SUBST: {h}\n")
        snps.write(f"#MIN_ABUNDANCE: {m}\n")
        snps.write(f"#POS\tREF\tALT\tABUNDANCE\n")

    # Find alignment position of each reads in both sens (forward dans reverse)
    with open(reads, "r") as rd:
        i_r = 0  # the index of read starts at 1

        print(f"Begins to align reads at {round(time.time() - t0, 3)} seconds.\n")

        list_read_size = []  # To have a reads size average
        size_genome = len(genome)

        for c_read in rd:
            if c_read[0] == ">":
                continue
            c_read = c_read.rstrip("\n")

            map_forward = mapping(c_read, bwt, sa, n, r, k, genome, h, i_r, "+")

            # Don't try to align the read reversed if the mapping of the forward read has no substitution (try if the mapping of forward is None or if it has substitutions)

            if map_forward is None:
                map_reverse = mapping(reverse(c_read), bwt, sa, n, r, k, genome, h, i_r, "-")
                # Takes the same criteria for selecting the best alignment
                best_map = select_align(map_reverse, map_forward)

            elif len(map_forward[1]) != 0:
                map_reverse = mapping(reverse(c_read), bwt, sa, n, r, k, genome, h, i_r, "-")
                best_map = select_align(map_reverse, map_forward)

            else:
                best_map = map_forward

            if best_map is None:
                i_r += 1
                continue

            # Stores the substitutions, if there are some.
            if len(best_map[1]) != 0:
                dict_snps = store_sub(c_read, best_map, dict_snps)

            # Go to the next read
            i_r += 1

            # For following the process
            follow_process(i_r, c_read, list_read_size, t0)

        print(f"\nReads alignment complete at {round(time.time() - t0, 3)} seconds with success, corresponding to {round((time.time()-t0)/60, 3)} minutes.\n")

    # Write the variant in the output file
    # For help to find the best m:
    dict_m = {}
    nb_sub = 0

    # To calculate the read size average :
    avg_size = 0
    if len(list_read_size) == 0 :
        raise ValueError(f"The argument k = {k} is too big, no one read aligned.")

    for c_size in list_read_size:
        avg_size += c_size

    avg_size = round(avg_size/len(list_read_size), 1)


    with open("snps.vcf", "a") as snps:
        for c_pos_sub in sorted(dict_snps.keys()):  # To have position of substitution sorted in the output file
            for c_base in sorted(dict_snps[c_pos_sub].keys()):
                if dict_snps[c_pos_sub][c_base] >= int(m):  # To have only variants with a abundance > to the threshold minimum
                    snps.write(f"{c_pos_sub}\t{genome[c_pos_sub]}\t{c_base}\t{dict_snps[c_pos_sub][c_base]}\n")
                    if dict_snps[c_pos_sub][c_base] not in dict_m.keys():
                        dict_m[dict_snps[c_pos_sub][c_base]] = 1
                    dict_m[dict_snps[c_pos_sub][c_base]] += 1
                    nb_sub += 1
    print("Data on this execution : ")
    print(f"Arguments used : k = {k}, h = {h}, m = {m}")
    print(f"Substitutions stored : {nb_sub}\nGenome of reference size : {size_genome} nucleotides\nNumber of reads : {i_r}")
    print(f"Average reads' size : {avg_size} nucleotides\nPercentage of identity : {round(((len(genome) - nb_sub) / len(genome))*100, 2)} %")
    deepth = round((i_r * avg_size) / size_genome, 1)
    print(f"Estimated deepth : {deepth} reads\n")
    print("_____________________________________________________________________")
    print(f"ABUNDANCE (int)\tABUNDANCE//DEEPTH\tPROPORTION OF SUBSTITUTION (%)\n")
    for c_abundance in sorted(dict_m.keys()):
        print(f"{c_abundance}\t{round(c_abundance/deepth, 2)}\t{round(((dict_m[c_abundance]/nb_sub)*100), 5)}")
    print("_____________________________________________________________________\n")


if __name__ == "__main__":
    t0 = time.time()
    main()
    t1 = time.time()
    print(f"The mapping and SNPS calling took {round(t1-t0, 3)} seconds, corresponding to {round((t1-t0)/60, 3)} minutes.\n")
